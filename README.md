# Some Reproducibility - Example

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/robit.a%2Fsome-reproducibility-example/master?urlpath=rstudio)


Example project for Some Reproducibility workshop, split off into separate repository for Binder
